package eu.europeana.challenge.math;

import org.junit.Assert;
import org.junit.Test;

public class LeastCommonMultipleTest {

    @Test
    public void CasesTest() {
        LeastCommonMultiple lcm = new LeastCommonMultiple();

        Assert.assertEquals(2520, lcm.getLeastCommonMultiple(1, 10).getValue());

        Assert.assertEquals(360360, lcm.getLeastCommonMultiple(1, 15).getValue());

        Assert.assertEquals(-2135207472, lcm.getLeastCommonMultiple(1, 25).getValue());
    }

}
