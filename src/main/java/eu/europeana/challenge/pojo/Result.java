package eu.europeana.challenge.pojo;

import lombok.Data;

@Data
public class Result {
    private int start;
    private int end;
    private int value;
    private boolean isValidInput;
    private String message;
}
