package eu.europeana.challenge.math;

import eu.europeana.challenge.pojo.Result;
import eu.europeana.challenge.util.ArrayUtil;

import java.util.Arrays;
import java.util.OptionalInt;

public class LeastCommonMultiple {
    public LeastCommonMultiple() {

    }

    /**
     * Calculate the least common multiple using an array of numbers
     * @param start start of the array
     * @param end the end of the array
     * @return An object with the least common multiple, if something went wrong see the message in the object
     */
    public Result getLeastCommonMultiple(int start, int end) {
        Result result = Validate(start, end);

        if(!result.isValidInput()) {
            return result;
        }

        int[] array = ArrayUtil.generateIntArray(result.getStart(), result.getEnd());

        OptionalInt value = Arrays.stream(array).reduce((p, q) -> calculate(p, q));

        if(value.isPresent()) {
            result.setValue(value.getAsInt());
        } else {
            result.setMessage("No solution present");
        }

        return result;
    }

    private int calculate(int p, int q) {
        int lcm = p / Euclid.gcd(p,q) * q;
        return lcm;
    }

    /**
     * Validates the input values
     * @param start start input
     * @param end end input
     * @return
     */
    private Result Validate(int start, int end) {
        Result result = new Result();
        result.setValidInput(true);

        if(start == end) {
            result.setValidInput(false);
            result.setMessage("Start and end number cannot be the same");
            return result;
        }

        if(start < 1 || end < 1) {
            result.setValidInput(false);
            result.setMessage("Start and end number must be equal or bigger than 1");
            return result;
        }

        if(start > end) {
            //The values are correct but in the wrong order
            result.setStart(end);
            result.setEnd(start);
        } else {
            result.setStart(start);
            result.setEnd(end);
        }

        return result;
    }
}
