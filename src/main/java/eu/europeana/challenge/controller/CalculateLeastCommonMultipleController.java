package eu.europeana.challenge.controller;

import eu.europeana.challenge.math.LeastCommonMultiple;
import eu.europeana.challenge.pojo.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("lcm")
public class CalculateLeastCommonMultipleController {
    private final Logger logger = LoggerFactory.getLogger(CalculateLeastCommonMultipleController.class);
    LeastCommonMultiple lcm = new LeastCommonMultiple();

    /**
     * Calculates the Least Common Multiple of an array
     * @return An object with the least common multiple, if something went wrong see the message in the object
     */
    @RequestMapping(value = "calculate", method = RequestMethod.GET,  produces = {MediaType.APPLICATION_XML_VALUE,
            MediaType.APPLICATION_JSON_VALUE})
    public Result calculate(HttpServletRequest request) {
        int end;
        try {
            end = (int) request.getSession().getAttribute("end");
        } catch (NullPointerException ex) {
            logger.error("End value is empty");
        } finally {
            end = 10;
        }

        logger.info("Calculating Least Common Multiplier");
        return lcm.getLeastCommonMultiple(1, end);
    }
}
