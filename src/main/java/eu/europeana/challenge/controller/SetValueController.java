package eu.europeana.challenge.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("value")
public class SetValueController {
    private final Logger logger = LoggerFactory.getLogger(SetValueController.class);

    /***
     * Sets the value for the calculate least common multiple
     * @param end the end number of the array
     */
    @RequestMapping(value = "setvalue", method = RequestMethod.GET)
    public void setValue(@RequestParam(value="upperValue", defaultValue="10") int end, HttpServletRequest request) {
        logger.info("Storing end number: " + end);
        request.getSession().setAttribute("end", end);
    }

}
