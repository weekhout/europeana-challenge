package eu.europeana.challenge.util;

import java.util.Arrays;
import java.util.stream.IntStream;

public class ArrayUtil {

    /**
     * Generates an array from start to finish sequentially.
     * @param start
     * @param end
     * @return
     */
    public static int[] generateIntArray(int start, int end) {
        int[] arr = new int[end];
        Arrays.setAll(arr, (index) -> start + index);
        return arr;
    }
}
