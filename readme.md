# Europeana challenge
Europeana gave me the following challenge for a job application. Which resulted in a Spring Rest Api that offers to 
solve the Least Common Multiple problem. 

## Challenge
Find the smallest number that can be divided by a sequential set of numbers:

2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
360360 is the smallest number that can be divided by each of the numbers from 1 to 15 without any remainder.
And so on..

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 25?

The application should be wrapped around two REST controllers, one to set the upper number to check(e.g. 25).
And another controller to start the calculation reporting back in json and xml(possible to choose from Accept header) the result and the time spend doing the calculation.

The candidate should create a java project.
The candidate should use and follow as close as possible the below:
1. Use of maven
2. Use of Spring Controllers and demonstrate some dependency injection, using annotations only
3. Structured logging ideally using slf4j-log4j, but any other framework is acceptable.
4. Junit tests
5. The faster the application calculates the result the better(e.g. 30secs as upper limit)
6. README file with clear instructions on how to build and execute the application

# Requirements
* Java 8

# Running
To get the application started do the following:
* Run the main application in the ```Application``` class
* Browse to http://localhost:8080

# Example
The example is in two steps; 1) set upper value and, 2) Calculate the Least Common Multiple. 
These actions can be done by:

1. Setting the value using GET: ```http://localhost:8080/value/setvalue?upperValue=```
2. Calculating the Least Common Multiple: ```http://localhost:8080/lcm/calculate```, this returns an object with the 
result

# Least Common Multiplier
The smallest positive number that is evenly divided (divided without remainder) by a set of numbers is called the Least Common Multiple (LCM). All we have to do is find the LCM for the integers {1, 2, 3, 4, …, 20} using Euclid’s algorithm.

After some reflection you might realize correctly that every integer is divisible by 1, so 1 can be removed from the list and calculate 2 through 20.

But by furthering this reasoning and we can eliminate other factors. We leave 20 in the calculation but then remove its factors {2, 4, 5, 10}. Any number evenly divisible by 20 is also evenly divisible by these factors. 19 is prime and has no factors – it stays. 18 has factors {2, 3, 6, 9} and we already removed 2 but we can remove 3, 6, and 9. 17 is prime – it stays too.

We continue this numeric carnage until our original list of {1..20} becomes the much smaller {11, 12, 13, 14, 15, 16, 17, 18, 19, 20}. Following this reasoning, the lower half of the set is completely irrelevant and can be eliminated from consideration without changing the result.

```Java
OptionalInt value = Arrays.stream(array).reduce((p, q) -> calculate(p, q));


private int calculate(int p, int q) {
    int lcm = p / Euclid.gcd(p,q) * q;
    return lcm;
}
```

# Author
Wouter Eekhout